<?php
class ChambreController
{
    private $instanceModelHotel;
    private $instanceModelChambre;
    private $instanceModelStation;


    public function __construct(){
        include("Model/HotelModel.php");
        include("Model/ReservationModel.php");
        include("Model/ChambreModel.php");
        $this->instanceModelHotel = new HotelModel();
        $this->instanceModelChambre = new ChambreModel();
        $this->instanceModelReservation = new ReservationModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }
    public function afficherChambres($noHotel)
    {
        $donnees=$this->instanceModelChambre->findAllChambresByHotel($noHotel);
        $donnees2=$this->instanceModelChambre->findDetailsAllChambresByHotel($noHotel);
        echo $this->twig->render('chambre/showChambres.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 ]);
    }
    public function creerChambre($noHotel)
    {
        $donnees['etat']='NEUF';
        $donnees['dateAchat']=date('d/m/Y');
        $donnees['noHotel'] = htmlentities($noHotel);
        $donnees2=$this->instanceModelChambre->findDetailsAllChambresByHotel($noHotel);
      //  dump($donnees);
        echo $this->twig->render('chambre/addChambre.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 ]);
    }

    public function validFormCreerChambre()
    {
        $donnees['etat'] = $_POST['etat'];
        $donnees['dateAchat'] = htmlentities($_POST['dateAchat']);
        $donnees['prix'] = htmlentities($_POST['prix']);
        $donnees['noHotel'] = htmlentities($_POST['noHotel']);

        $erreurs=$this->validatorChambre($donnees);
        if(empty($erreurs)) {
            $donnees['dateAchat_us'] = DateTime::createFromFormat('d/m/Y', $donnees['dateAchat'])->format('Y-m-d');
            $this->instanceModelChambre->createAndPersistChambre($donnees);
            $URI='/Chambre/afficherChambres/'.$donnees['noHotel'];
            header("Location: ".BASE_URL.$URI);
        }
        $donnees2=$this->instanceModelChambre->findDetailsAllChambresByHotel($donnees['noHotel']);
        echo $this->twig->render('chambre/addChambre.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 , 'erreurs' => $erreurs]);
    }

    public function supprimerChambre($noChambre='')
    {
        $dataReservationChambre=$this->instanceModelReservation->findByIdChambreReservation($noChambre);
        $dataChambre=$this->instanceModelChambre->findOneById($noChambre);

        if(empty($dataReservationChambre)){
            $this->instanceModelChambre->removeByIdChambre($noChambre);
            $URI='/Chambre/afficherChambres/'.$dataChambre['noHotel'];
            header("Location: ".BASE_URL.$URI);
        }
        echo $this->twig->render('chambre/ErrorDeleteChambre.html.twig',['reservationsChambre' => $dataReservationChambre, 'chambre' => $dataChambre]);
    }



    public function modifierChambre($noChambre='')
    {
        $donnees=$this->instanceModelChambre->findOneById($noChambre);
        $donnees2=$this->instanceModelChambre->findDetailsAllChambresByHotel($donnees['noHotel']);
        if($donnees['dateAchat']){
            list($year,$month, $day )  = explode('-', $donnees['dateAchat']);
            $donnees['dateAchat']=$day."/".$month."/".$year;
        }
        echo $this->twig->render('chambre/editChambre.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 ]);
    }

    public function validFormModifierChambre()
    {
        $donnees['noHotel']=$_POST['noHotel'];
        // ## contrôles des données
        $donnees['noChambre']=$_POST['noChambre'];
        $donnees['etat']=htmlentities($_POST['etat']);
        $donnees['dateAchat']=htmlentities($_POST['dateAchat']);
        $donnees['prix']=htmlentities($_POST['prix']);


        $erreurs=$this->validatorChambre($donnees);
        if(empty($erreurs)) {
            $donnees['dateAchat_us'] = DateTime::createFromFormat('m/d/Y', $donnees['dateAchat'])->format('Y-m-d');
            $this->instanceModelChambre->updateAndPersistChambre($donnees['noChambre'], $donnees);
            $URI='/Chambre/afficherChambres/'.$donnees['noHotel'];
            header("Location: ".BASE_URL.$URI);
        }
        $donnees2=$this->instanceModelChambre->findDetailsAllChambresByHotel($donnees['noHotel']);
        dump($donnees);       dump($donnees2);dump($erreurs);
        echo $this->twig->render('chambre/editChambre.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 , 'erreurs' => $erreurs]);

    }


    public function validatorChambre($donnees)
    {
        $erreurs=array();
        if (!preg_match("/^[0-9.]{1,}$/", $donnees['prix'])) $erreurs['prix'] = 'saisir un prix au format dd.dd';

        $dateConvert=DateTime::createFromFormat('d/m/Y',$donnees['dateAchat']);
        if($dateConvert==NULL)
            $erreurs['dateAchat']='la date doit être au format JJ/MM/AAAA';
        else{
            if($dateConvert->format('d/m/Y') !== $donnees['dateAchat']){
                $erreurs['dateAchat']='la date n\'est pas valide (format JJ/MM/AAAA)';
              //  var_dump($dateConvert->format('d/m/Y'));var_dump($donnees['dateAchat']);
            }

        }
        return $erreurs;
    }

}






<?php
class ClientController
{
    private $instanceModelReservation;
    private $instanceModelClient;

    public function __construct(){
        include("Model/ReservationModel.php");
        include("Model/ClientModel.php");
        $this->instanceModelReservation = new ReservationModel();
        $this->instanceModelClient = new ClientModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }

    public function afficherClients()
    {
        $clients=$this->instanceModelClient->findAllClients();
        echo $this->twig->render('client/showClients.html.twig',['clients' => $clients]);
    }

    public function creerClient()
    {
        echo $this->twig->render('client/addClient.html.twig');
    }

    public function validFormCreerClient()
    {
        $donnees['nomClient']=$_POST['nomClient'];
        $donnees['adresse']=$_POST['adresse'];
        $donnees['telephone']=$_POST['telephone'];
        $donnees['dateNaissance']=$_POST['dateNaissance'];
        $erreurs=$this->validatorClient($donnees);
        if(empty($erreurs)){
            $donnees['nomClient'] = addslashes($donnees['nomClient']);
            $donnees['dateNaissance_us'] = DateTime::createFromFormat('d/m/Y', $donnees['dateNaissance'])->format('Y-m-d');
            $this->instanceModelClient->createAndPersistClient($donnees);
            header("Location: ".BASE_URL."/Client/afficherClients");
        }
        echo $this->twig->render('client/addClient.html.twig',['erreurs'=>$erreurs, 'donnees'=>$donnees]);
    }

    public function supprimerClient($id='')
    {
        $donnees = $this->instanceModelReservation->findByIdClientReservation($id);
        $nombre = count($donnees);
        if(empty($donnees)){
            $this -> instanceModelClient->removeByIdClient($id);
            header("Location: ".BASE_URL."/Client/afficherClients");
        }
        echo $this->twig->render('client/ErrorDeleteClient.html.twig',['nombre' => $nombre]);

     
    }

    public function modifierClient($id='')
    {
        $client=$this->instanceModelClient->findOneByIdClient($id);
        $client['dateNaissance']=DateTime::createFromFormat('Y-m-d', $client['dateNaissance'])->format('d/m/Y');
        echo $this->twig->render('client/editClient.html.twig',['donnees' => $client]);
    }

    public function validFormModifierClient()
    {
        $donnees['nomClient']=addslashes($_POST['nomClient']);
        $donnees['adresse']=htmlentities($_POST['adresse']);
        $donnees['telephone']=htmlentities($_POST['telephone']);
        $donnees['dateNaissance']=htmlentities($_POST['dateNaissance']);
        $donnees['idClient']=htmlentities($_POST['idClient']);

        $erreurs=$this->validatorClient($donnees);
        if(empty($erreurs)){
            $donnees['dateNaissance_us'] = DateTime::createFromFormat('d/m/Y', $donnees['dateNaissance'])->format('Y-m-d');
            $this->instanceModelClient->updateAndPersistClient($donnees['idClient'],$donnees);
            header("Location: ".BASE_URL."/Client/afficherClients");
        }
        echo $this->twig->render('client/editClient.html.twig',['erreurs' => $erreurs, 'donnees' => $donnees]);
    }


    public function validatorClient($donnees)
    {
        $erreurs=array();
        if(!preg_match("/^[A-Za-z]{1,}/", $donnees['nomClient']))
            $erreurs['nomClient'] = 'nom composé de 2 lettres minimum';
        if (!preg_match("/^[A-Za-z0-9]{1,}/", $donnees['adresse']))
            $erreurs['adresse'] = 'adresse composé de 2 lettres minimum';
        if (!preg_match("/^[0-9]{10}/", $donnees['telephone']))
            $erreurs['telephone'] = 'telephone composé de 10 chiffres';

        $dateConvert = DateTime::createFromFormat('d/m/Y', $donnees['dateNaissance']);
        if ($dateConvert == NULL)
            $erreurs['dateNaissance']='la date doit être au format JJ/MM/AAAA';
        else{
            if ($dateConvert->format('d/m/Y') !== $donnees['dateNaissance'])
                $erreurs['dateNaissance']='la date n\' est pas valide (format jj/mm/aaaa)';
        }
        return $erreurs;
    }

}




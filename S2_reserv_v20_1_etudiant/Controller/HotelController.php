<?php
class HotelController
{
    private $instanceModelHotel;
    private $instanceModelChambre;
    private $instanceModelStation;

    public function __construct(){
        include("Model/HotelModel.php");
        include("Model/StationModel.php");
        include("Model/ChambreModel.php");
        $this->instanceModelHotel = new HotelModel();
        $this->instanceModelChambre = new ChambreModel();
        $this->instanceModelStation = new StationModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }
    public function afficherHotels()
    {
        $hotels=$this->instanceModelHotel->findAllHotels();
        echo $this->twig->render('hotel/showHotel.html.twig',['hotels' => $hotels]);
    }
    public function creerHotel()
    {
       
    }

    public function validFormCreerHotel()
    {
        
    }

    public function supprimerHotel($id='')
    {
       
    }



    public function modifierHotel($id='')
    {

    }

    public function validFormModifierHotel()
    {
  
    }


    public function validatorHotel($donnees)
    {
        $erreurs=array();

        return $erreurs;
    }

}



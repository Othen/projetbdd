<?php
class ReservationController
{
    private $instanceModelClient;
    private $instanceModelChambre;
    private $instanceModelReservation;


    public function __construct(){
        include("Model/ClientModel.php");
        include("Model/ReservationModel.php");
        include("Model/ChambreModel.php");
        $this->instanceModelClient = new ClientModel();
        $this->instanceModelChambre = new ChambreModel();
        $this->instanceModelReservation = new ReservationModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }

    // addEmpunts

    public function selectClientReservations()
    {
        echo $this->twig->render('layout.html.twig',['error' => 'selectClientReservations : action manquante']);
    }

    public function addReservationsShowReservationClient()
    {
    }

    public function addReservations()
    {

        
    }

    // deleteAllEmpunts
    public function deleteAllReservations()
    {
     
    }

    public function validFormAddDeleteAllReservations($id='')
    {

  
    }

    //returnReservations

    public function returnReservations($id='')
    {
   
    }

    public function bilanReservations()
    {

    }


    public function validatorReservation($donnees)
    {
        $erreurs=array();
      
        return $erreurs;
    }

    public function validatorRetour($donnees)
    {
        $erreurs=array();
    
        return $erreurs;
    }

}





<?php
class StationController
{
    private $instanceModelHotel;
    private $instanceModelStation;

    public function __construct(){
        include("Model/HotelModel.php");
        include("Model/StationModel.php");
        include("Model/ChambreModel.php");
        $this->instanceModelHotel = new HotelModel();
        $this->instanceModelStation = new StationModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }

    public function afficherStations()
    {
        $stations = $this->instanceModelStation->findAllStations();
        echo $this->twig->render('station/showStations.html.twig',['stations' => $stations]);
    }

    public function creerStation()
    {
        echo $this->twig->render('station/addStation.html.twig');
    }

    public function validFormCreerStation()
    {
        $donnees['nomStation']=$_POST['nomStation'];
        $donnees['altitude']=$_POST['altitude'];
        $donnees['region']=$_POST['region'];
        $erreurs=$this->validatorStation($donnees);
        if(empty($erreurs)){
            $donnees['nomStation'] = addslashes($donnees['nomStation']);
            $this->instanceModelStation->createAndPersistStation($donnees);
            header("Location: ".BASE_URL."/Station/afficherStations");
        }
        echo $this->twig->render('station/addStation.html.twig',['erreurs'=>$erreurs, 'donnees'=>$donnees]);
    }

    public function supprimerStation($id='')
    {
        $donnees = $this->instanceModelHotel->findByIdStationHotel($id);
        $nombre = count($donnees);
        if(empty($donnees)){
            $this -> instanceModelStation->removeByIdStation($id);
            header("Location: ".BASE_URL."/Station/afficherStations");
        }
         echo $this->twig->render('station/ErrorDeleteStation.html.twig',['nombre' => $nombre]);
    }

    public function modifierStation($id='')
    {
        $station=$this->instanceModelStation->findOneByIdStation($id);
        echo $this->twig->render('station/editStation.html.twig',['donnees' => $station]);
    }

    public function validFormModifierStation()
    {
        $donnees['nomStation']=addslashes($_POST['nomStation']);
        $donnees['altitude']=htmlentities($_POST['altitude']);
        $donnees['region']=htmlentities($_POST['region']);
        $donnees['idStation']=htmlentities($_POST['idStation']);

        $erreurs=$this->validatorStation($donnees);
        if(empty($erreurs)){
            $this->instanceModelStation->updateAndPersistStation($donnees['idStation'],$donnees);
            header("Location: ".BASE_URL."/Station/afficherStations");
        }
        echo $this->twig->render('station/editStation.html.twig',['erreurs' => $erreurs, 'donnees' => $donnees]);
    }


    public function validatorStation($donnees)
    {
        $erreurs=array();
        if(!preg_match("/^[A-Za-z]{1,}/", $donnees['nomStation']))
            $erreurs['nomStation'] = 'nom composé de 2 lettres minimum';
        if (!preg_match("/^[A-Za-z]{1,}/", $donnees['region']))
            $erreurs['region'] = 'nom composé de 2 lettres minimum';
        if (!preg_match("/^[0-9]/", $donnees['altitude']))
            $erreurs['altitude'] = 'altitude composé de chiffres';
        return $erreurs;
    }

}




<?php

class ClientModel{

    private $db;

    public function __construct(){
        $MaConnexion = new Connexion();
        $this->db = $MaConnexion->connect();
    }

    function findAllClients(){
        $requete = "
        SELECT client.nomClient
        , client.adresse
        , client.telephone
        , client.dateNaissance
        , client.idClient
        , count(reservation.idClient) AS nbrReserv
        , IF(CURRENT_DATE() > reservation.dateFin,1,0) AS terminee
        , IF(CURRENT_DATE() < reservation.dateDebut,1,0) AS future
        , IF(CURRENT_DATE() >= reservation.dateDebut
        AND CURRENT_DATE() <= reservation.dateFin,1,0) AS enCours
        FROM client 
        LEFT JOIN reservation ON reservation.idClient = client.idClient
        GROUP BY client.idClient;";
        $select = $this->db->query($requete);
        $results = $select->fetchAll();
        return $results;
    }

    function findAllDropdownClients(){
       
    }

    function createAndPersistClient($donnees){
        $requete="INSERT INTO Client (idClient, nomClient, adresse, telephone, dateNaissance) 
        VALUES (NULL,'${donnees['nomClient']}','${donnees['adresse']}','${donnees['telephone']}','${donnees['dateNaissance_us']}');";
        $nbRes = $this->db->exec($requete);
    }


    function removeByIdClient($id){
        $requete="DELETE
        FROM Client WHERE idClient=".$id." LIMIT 1;";
        $nbRes = $this->db->exec($requete);
        return $nbRes;
    }

    function findOneByIdClient($id){
        $requete="SELECT idClient, nomClient, adresse, telephone, dateNaissance
        FROM client
        WHERE idClient =:id";
        $prep=$this->db->prepare($requete);
        $prep->bindParam(":id", $id, PDO::PARAM_INT);
        $prep->execute();
        $result = $prep->fetch();
        return $result;
    }
    function updateAndPersistClient($id, $donnees){
        $requete="UPDATE client
        SET nomClient = '${donnees['nomClient']}'
        , adresse='${donnees['adresse']}'
        , telephone='${donnees['telephone']}'
        , dateNaissance='${donnees['dateNaissance_us']}'
        WHERE idClient = ".$id.";";
        $nbRes = $this->db->exec($requete);
    }


}


<?php


class HotelModel{

    private $db;

    public function __construct(){
        $MaConnexion = new Connexion();
        $this->db = $MaConnexion->connect();
    }

    function findAllHotels(){
        $requete = "
        SELECT station.nomStation
        , hotel.nomHotel
        , hotel.categorie
        , hotel.dateConstruction
        , count(chambre.numChambre) AS nbrChambres
        , IF ()
        FROM hotel
        INNER JOIN station ON hotel.idStation = station.idStation
        INNER JOIN chambre ON hotel.idHotel = chambre.idHotel
        GROUP BY hotel.idHotel
        ORDER BY station.nomStation;";
        $select = $this->db->query($requete);
        $results = $select->fetchAll();
        return $results;
    }

    function findByIdStationHotel($id){
        $requete = "SELECT*FROM hotel WHERE idStation = ".$id.";";
        $select = $this->db->query($requete);
        $results = $select->fetchAll();
        return $results;
    }

    function createAndPersistHotel($donnees){

    }


    function removeByIdHotel($id){

    }

    function findOneByIdHotel($id){

    }

    function updateAndPersistHotel($id, $donnees){

    }
}

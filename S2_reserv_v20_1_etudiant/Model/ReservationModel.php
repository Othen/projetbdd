<?php

class ReservationModel{

    private $db;

    public function __construct(){
        $MaConnexion = new Connexion();
        $this->db = $MaConnexion->connect();
    }

    function createAndPersistReservation($donnees){

    }


    function updateAndPersistReservation($donnees){

    }

    // chambres des hotels disponibles

    function findChambreHotelDispo(){

    }


    function removeByIdReservation($donnees){

    }


    function findClientReservation($idClient=""){

    }


    function findByIdClientReservation($idClient){
        $requete = "SELECT*FROM reservation WHERE idClient = ".$idClient.";";
        $select = $this->db->query($requete);
        $results = $select->fetchAll();
        return $results;
    }

    function findByIdChambreReservation($noChambre){
    
    }

    // nombre d'chambres en retard pour un adhérent
    function findNbExempairesRetardClient($idClient){

    }

    // chambres des hotels disponibles
    function findChambresArendre($idClient){

    }


    ////////// pour reservationer un livre


    // liste des adhérents ayant le droit d'reservationer un livre (pour la liste déroulante des adhérents lors d'un reservation

    function findReservationDropdownClients(){

    }

    // liste des adhérents ayant le droit d'reservationer un livre (pour la liste déroulante des adhérents lors d'un reservation

    function findReservationsByOneClient($idClient){

    }

    // liste des adhérents ayant le droit d'reservationer un livre (pour la liste déroulante des adhérents lors d'un reservation

    function findReservationReturnDropdownClients(){

    }

    // bilan

    function findReservationsBilan(){

    }

}


<?php

class StationModel{

    private $db;

    public function __construct(){
        $MaConnexion = new Connexion();
        $this->db = $MaConnexion->connect();
    }

    function findAllStations(){
        $requete = "
        SELECT station.nomStation
        , station.altitude
        , station.region
        , station.idStation
        , count(hotel.idHotel) as nbrHotel
        FROM hotel
        RIGHT JOIN station ON station.idStation = hotel.idStation
        GROUP BY station.nomStation, station.altitude, station.region, station.idStation
        ORDER BY station.nomStation;
        ";
        $select = $this->db->query($requete);
        $results = $select->fetchAll();
        return $results;
    }


    function createAndPersistStation($donnees){
        $requete="INSERT INTO Station (idStation, nomStation, altitude, region) 
        VALUES (NULL,'${donnees['nomStation']}','${donnees['altitude']}','${donnees['region']}');";
        $nbRes = $this->db->exec($requete);
    }


    function removeByIdStation($id){
        $requete="DELETE
        FROM Station WHERE idStation=".$id." LIMIT 1;";
        $nbRes = $this->db->exec($requete);
        return $nbRes;
    }

    function findOneByIdStation($id){
        $requete="SELECT idStation, nomStation, altitude, region
        FROM station
        WHERE idStation =:id";
        $prep=$this->db->prepare($requete);
        $prep->bindParam(":id", $id, PDO::PARAM_INT);
        $prep->execute();
        $result = $prep->fetch();
        return $result;
    }
    function updateAndPersistStation($id, $donnees){
        $requete="UPDATE station
        SET nomStation = '${donnees['nomStation']}'
        , altitude='${donnees['altitude']}'
        ,region='${donnees['region']}'
        WHERE idStation =".$id.";";
        $nbRes = $this->db->exec($requete);
    }


    // liste déroulante add/edit HOTEL
    function findAllDropdownStations(){
        return NULL;
    }
}
